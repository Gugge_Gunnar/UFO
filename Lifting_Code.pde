float cur_X = 0;
float cur_Y = 0;
float cur_dir = 0;

void goTo(float X,float Y){
  rotate(-cur_dir);
  translate(-cur_X,-cur_Y);
  cur_X = X;
  cur_Y = Y;
  translate(cur_X,cur_Y);
  rotate(cur_dir);
}
void moveStepsInDir(float Steps,float Dir){
  translate(-cur_X,-cur_Y);
  cur_X += Steps*radians(sin(Dir));
  cur_Y += Steps*radians(cos(Dir));
  translate(cur_X,cur_Y);
}
void changeXY(float X,float Y){
  rotate(-cur_dir);
  translate(-cur_X,-cur_Y);
  cur_X += X;
  cur_Y += Y;
  translate(cur_X,cur_Y);
  rotate(cur_dir);
}
void turn(float Amount){
  rotate(-cur_dir);
  cur_dir = Amount;
  rotate(cur_dir);
}
void resetDir(){
  rotate(-cur_dir);
  cur_dir = 0;
}
void makeImage(PImage image,float X,float Y){
  fill(0,126);
tint(0, 126); 
rotate(-cur_dir);
translate(5,5);
rotate(cur_dir);
image(image,X,Y,image.width,image.height);
rotate(-cur_dir);
translate(-5,-5);
rotate(cur_dir);
  fill(255);
  noTint();
image(image,X,Y,image.width,image.height);
}
void makeImage(PImage image,float X,float Y,float W,float H){
  fill(0,126);
tint(0, 126); 
rotate(-cur_dir);
translate(5,5);
rotate(cur_dir);
image(image,X,Y,W,H);
rotate(-cur_dir);
translate(-5,-5);
rotate(cur_dir);
  fill(255);
  noTint();
image(image,X,Y,W,H);
}
void makeText(String text,float X,float Y){
  fill(0,126);
tint(0, 126); 
rotate(-cur_dir);
translate(5,5);
rotate(cur_dir);
text(text,X,Y);
rotate(-cur_dir);
translate(-5,-5);
rotate(cur_dir);
  fill(255);
  noTint();
text(text,X,Y);
}
void makeText(float text,float X,float Y){
  fill(0,126);
tint(0, 126); 
rotate(-cur_dir);
translate(5,5);
rotate(cur_dir);
text(text,X,Y);
rotate(-cur_dir);
translate(-5,-5);
rotate(cur_dir);
  fill(255);
  noTint();
text(text,X,Y);
}
