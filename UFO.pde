import net.java.games.input.*;
import org.gamecontrolplus.*;
import org.gamecontrolplus.gui.*;
ControlIO control;
ControlDevice stick;
String inputType = "XBox";
float ShipMode = 1;

import ddf.minim.*;

Minim minim;
AudioPlayer player;

boolean start = true;
float startY = 0;
float startTime = 100;
PImage Makerspace;

PImage Background;
PImage SpaceStation;
PImage Asteroid;
PImage Bullet;
PImage UFO;

PImage Gameover;
float gy = -200;
float gs = 0;
float gravity = 0;
int Bounces;

PImage Hearts;
PImage Lost;
float h1 = 225;
float h2 = 300;
float h3 = 375;
float hp = 3;

float x1 = 0;
float y1 = 0;
float ms = 0;
int points = 0;
PFont Bold;
PFont Futuristic;
float Inv = 0;

PImage BSS;
float BSSY = -2000;
float SSY = 40;

float[] astX = { };
float[] astY = { };
float[] astHP = { };
boolean end = true;
int array = -1;
float astspeed = 0;
float astdifficulty = 0;
float JoyY = 1;
float JoyX = 1;
float[] BulletsX = { };
float[] BulletsY = { };
float[] BulletsXV = { };
float[] BulletsYV = { };
float[] BulletsHP = { };
int Bullets = 0;
float fireDelay = 0;

float[] BombsX = { };
float[] BombsY = { };
float[] BombsHP = { };
float[] DroneX = { };
float[] DroneY = { };
float[] DroneHP = { };
float[] DroneID = { };
int Bombs = 0;
int Drone = 0;
float fireDelay2,fireDelay3,fireDelay4,fireDelay5 = 0;
float[] ExpX = { };
float[] ExpY = { };
float[] ExpHP = { };
int Explosions = 0;
float LaserTarget = 0;
float tempCount = 0;
PImage Bomb;
PImage Explosion;
PImage SSEC0;
PImage SSEC1;
PImage SSEC2;
float SSEC = 0;
float OSS = 0;
float moveDir = 3;

PImage Laser;
float LaserX;
boolean FireDisable = true;

boolean FireDisable2 = true;
float stun;
float stuntime;
float shield;
PImage Shield;
PImage ShieldInactive;
boolean setup = false;
int TP;


void setup()
{
  //Save(2);
  //Save(1);
  fullScreen();
  Background = loadImage("Space.jpg");
  SpaceStation = loadImage("Space station.png");
  BSS = loadImage("Broken Space station.png");
  Asteroid = loadImage("Asteroid.png");
  Bullet = loadImage("Bullet.png");
  UFO = loadImage("UFO.png");
  Hearts = loadImage("Heart.png");
  Lost = loadImage("Lost.png");
  Gameover = loadImage("Gameover.png");
  Bold = createFont("Roboto-Bold.ttf",1);
  Futuristic = createFont("Futuristic.ttf",1);
  Bomb= loadImage("Bomb.png");
  Explosion = loadImage("Explosion.png");
  Laser = loadImage("Laser.png");
  Shield = loadImage("Shield.png");
  ShieldInactive = loadImage("ShieldInactive.png");
  Makerspace = loadImage("Uppsala Makerspace.png");
  SSEC0 = loadImage("Electricity_Shake0.png");
  SSEC1 = loadImage("Electricity_Shake1.png");
  SSEC2 = loadImage("Electricity_Shake2.png");
  //if (startTime<10){
  minim = new Minim(this);
  minim = new Minim(this);
  player = minim.loadFile("groove.mp3");
  //}
  x1 = width/4;
  y1 = height/2;
  LaserX = -width*99;
  
  astX = new float [100000];
  astY = new float [100000];
  astHP = new float [100000];
  for (int i=0, array = 0; i<15; ++i,++array)
  {
    astX[array] = random(width+50,width+500);
    astY[array] = random(0+30,height-30);
    astHP[array] = 3;
  }
  BulletsX = new float [100000];
  BulletsY = new float [100000];
  BulletsXV = new float [100000];
  BulletsYV = new float [100000];
  BulletsHP = new float [100000];
  Bullets = 0;
  
  BombsX = new float [100000];
  BombsY = new float [100000];
  BombsHP = new float [100000];
  DroneX = new float [100000];
  DroneY = new float [100000];
  DroneHP = new float [100000];
  DroneID = new float [100000];
  Bombs = 0;
  Drone = 0;
  ExpX = new float [100000];
  ExpY = new float [100000];
  ExpHP = new float [100000];
  Explosions = 0;
  
  gy = -200;
  gs = 0;
  gravity = 0;
  h1 = 225;
  h2 = 300;
  h3 = 375;
  hp = 3;
  shield = 0;
  OSS = 0;
  SSEC = 0;
  stun = 0;
  fireDelay2 = 0;
  fireDelay3 = 0;
  fireDelay4 = 0;
  moveDir = 0;
  
        SSY = 40;
        BSSY = -2000;
        points = 0;
        ms = 5;
        end = false;
          x1 = width/4;
  y1 = height/2;
  LaserX = -width*99;
  TP = 0;
  
  if (!(setup)){
  surface.setTitle("Unimageable.Fantastic.Orbital: Space_Defence");
  control = ControlIO.getInstance(this);
  stick = control.filter(GCP.STICK).getMatchedDevice("DefaultInputsExample");
  if (stick == null) {
    println("No suitable device configured");
    System.exit(-1);
  }}
  setup = true;
  
}

// || Setup Scripts above || Control scripts underneath || || Setup Scripts above || Control scripts underneath || || Setup Scripts above || Control scripts underneath ||

boolean pressingKey(char Button){
  if (keyPressed){
    if ((key == Button)){
      return(true);
    }
  }
  return(false);
}

boolean ControlKey(String Button){
  if (inputType == "XBox" || inputType == "nextXBox"){
    if (stick.getButton("Input0X").pressed() && Button == "X"){
        return(true);
    }
    if (stick.getButton("Input1A").pressed() && Button == "A"){
        return(true);
    }
    if (stick.getButton("Input2B").pressed() && Button == "B"){
        return(true);
    }
    if (stick.getButton("Input3Y").pressed() && Button == "Y"){
        return(true);
    }
    if (stick.getButton("Input4L").pressed() && Button == "L"){
        return(true);
    }
    if (stick.getButton("Input6R").pressed() && Button == "R"){
        return(true);
    }
    if (stick.getButton("Input8SELECT").pressed() && Button == "Select"){
        return(true);
    }
    if (stick.getButton("Input9START").pressed() && Button == "Start"){
        return(true);
    }
  }
  if (inputType == "SNES" || inputType == "nextSNES"){
    if (stick.getButton("Input0X").pressed() && Button == "X"){
        return(true);
    }
    if (stick.getButton("Input1A").pressed() && Button == "A"){
        return(true);
    }
    if (stick.getButton("Input2B").pressed() && Button == "B"){
        return(true);
    }
    if (stick.getButton("Input3Y").pressed() && Button == "Y"){
        return(true);
    }
    if (stick.getButton("Input4L").pressed() && Button == "L"){
        return(true);
    }
    if (stick.getButton("Input6R").pressed() && Button == "R"){
        return(true);
    }
    if (stick.getButton("Input8SELECT").pressed() && Button == "Select"){
        return(true);
    }
    if (stick.getButton("Input9START").pressed() && Button == "Start"){
        return(true);
    }
  }
  return(false);
}

// || Control Scripts above || Draw scripts underneath || || Control Scripts above || Draw scripts underneath || || Control Scripts above || Draw scripts underneath ||

void draw(){
  if (pressingKey('k')){
    if (inputType == "XBox") {
      inputType = "nextSNES";
    }
    if (inputType == "SNES") {
      inputType = "nextXBox";
    }
  }
  if (pressingKey('k') == false){
    if (inputType == "nextSNES") {
      inputType = "SNES";
    }
    if (inputType == "nextXBox") {
      inputType = "XBox";
    }
  }
  if (ControlKey("R")){
    if (ShipMode == 1) {
      ShipMode = 2.2;
    }
    if (ShipMode == 2) {
      ShipMode = 3.2;
    }
    if (ShipMode == 3) {
      ShipMode = 1.2;
    }
  }
  if (ControlKey("R") == false){
    if (ShipMode == 1.2) {
      ShipMode = 1;
    }
    if (ShipMode == 2.2) {
      ShipMode = 2;
    }
    if (ShipMode == 3.2) {
      ShipMode = 3;
    }
  }
    
  textSize(10);
  //print(int((map(stick.getSlider("JoystickY").getValue(), -1, 1, 0, 2)-1)*-1));
  JoyY = (map(stick.getSlider("JoystickY").getValue(), -1, 1, 0, 2)-1)*-1;
  JoyX = (map(stick.getSlider("JoystickX").getValue(), -1, 1, 0, 2)-1)*1;

  background(256,256,256);
  background(122.5);
  image(Background,0,0);
  makeImage(SpaceStation,-500,SSY);
  makeImage(BSS,-500,BSSY);
  Inv -= 0.05;
  
  cur_X = 0;
  cur_Y = 0;
  cur_dir = 0;

  //if (keyPressed)
  //{
    ms = ms/(SSEC/4+1);
    if (OSS>10){ms+=5;}
    if (!(TP > 0)){
    if (pressingKey('w') || (( JoyY > 0.5) ))y1 -= ms;
    if ((pressingKey('s')) || (( JoyY < -0.5) ))y1 += ms;
    if ((pressingKey('a')) || (( JoyX < -0.5) ))x1 -= ms;
    if ((pressingKey('d')) || (( JoyX > 0.5) ))x1 += ms;
    }
    if (sqrt((JoyX*JoyX)+(JoyY*JoyY))>0){
    moveDir = ((((((moveDir*-1)+180)%180)-180*-1)*-1+(PI/2))-degrees(atan2(JoyX,JoyY)))/3;}
    text(degrees(atan2(JoyX,JoyY)),100,100);
  //}
  ms = 5;
  if(x1<50&&end==false){x1=50;}
  if(x1+50>width&&end==false){x1=width-50;}
  if(y1<50&&end==false){y1=50;}
  if(y1+50>height&&end==false){y1=height-50;}
  
  shootScripts();
  
      if (ControlKey("Start")){/*
        SSY = 40;
        BSSY = -2000;
        points = 0;
        ms = 5;
        end = false;
          x1 = width/4;
  y1 = height/2;
  LaserX = -width*99;
  
  astX = new float [100000];
  astY = new float [100000];
  astHP = new float [100000];
    astspeed = 1;
    astdifficulty = 0.0005;
  for (int i=0, array = 0; i<15; ++i,++array)
  {
    astX[array] = random(width+50,width+500);
    astY[array] = random(0+30,height-30);
    astHP[array] = 3;
  }
  BulletsX = new float [100000];
  BulletsY = new float [100000];
  BulletsHP = new float [100000];
  
  BombsX = new float [100000];
  BombsY = new float [100000];
  BombsHP = new float [100000];
  ExpX = new float [100000];
  ExpY = new float [100000];
  ExpHP = new float [100000];
  gy = -200;
  gs = 0;
  gravity = 0;
  h1 = 225;
  h2 = 300;
  h3 = 375;
  hp = 3;
  shield = 0;
  OSS = 0;
  SSEC = 0;
  stun = 0;
  fireDelay2 = 0;
  fireDelay3 = 0;*/ //Why reset when you can use setup?
  setup();
  } 
  
  astspeed += astdifficulty;
  for (int i=0, array = 0; i<15; ++i,++array)
  {
    if (astHP[array] > 0) {
      astX[array] -= astspeed;
      //astY[array]--;
      goTo(astX[array],astY[array]);
      turn(radians(astY[array]));
      makeImage(Asteroid,0-50,0-50,100,100);
      turn(-radians(astY[array]));
      goTo(0,0);
    }
    else
    {
      astX[array] = random(width+50,width+500);
      astY[array] = random(0+30,height-30);
      astHP[array] = 3;
      points += 1;
    }
    if (astX[array] < 0)
    {
      Damage();
      astX[array] = random(width+50,width+500);
      astY[array] = random(0+30,height-30);
      astHP[array] = 3;
    }
    /*if (abs(astX[array]-x1)<70){
      if (abs(astY[array]-y1)<70){*/
      if (dist(astX[array],astY[array],x1,y1)<80){
        Damage();
        astX[array] = random(width+50,width+500);
        astY[array] = random(0+30,height-30);
    //  }
    }
  if (ShipMode == 1 || ShipMode == 1.2){
    if (LaserX > 0 && astX[array] > LaserX && abs(astY[array]-y1-10) < 45){
     astHP[array] -= 0.05; 
    }
    if (LaserX > 0 && astX[array] > LaserX && abs(astY[array]-y1+10) < 45){
     astHP[array] -= 0.05; 
    }
  }else if (ShipMode == 2 || ShipMode == 2.2){
  }
  }
      goTo(0,0);
  
  /*
  if(stick.getButton("Input0X").pressed()){text("X",10,10);}
  if(stick.getButton("Input1A").pressed()){text("A",10,20);}
  if(stick.getButton("Input2B").pressed()){text("B",10,30);}
  if(stick.getButton("Input3Y").pressed()){text("Y",10,40);}
  if(stick.getButton("Input4L").pressed()){text("L",10,50);}
  if(stick.getButton("Input6R").pressed()){text("R",10,60);}
  if(stick.getButton("Input8SELECT").pressed()){text("SELECT",10,70);}
  if(stick.getButton("Input9START").pressed()){text("START",10,80);}
  text(JoyY,10,100);
  text(JoyX,10,120);
  */
  
      goTo(0,0);
  resetDir();
  if (hp > 0){
  resetDir();
  bullet();
  resetDir();
  bomb();
  /*}else{
    fireDelay = 1;
    fireDelay2 = 1;*/
  }
    if (gy > height/3) {
    gs = gs * -0.8;
    gy = height/3;
    Bounces += 1;
  }
  if (Bounces == 5 && gy > 0){
    gravity = 0;
    gy = height/3;
    gs = 0;
  }
  textSize(50);
  fill(256,256,256);
  if(end == true){
  textAlign(CENTER);
  makeText("Press start to restart",width/2,height/3*2);
  }
  gy += gs;
  gs += gravity;
  makeImage(Gameover, width/2-(1112/2), gy);
  
  startTime -= 1;
  if(startTime < 0 && startTime > -10){
    start = false;
    FireDisable2 = false;
    startY = -height;
  }
  if (start == false){
    ms = 5;
    astspeed = 1;
    astdifficulty = 0.0005;
    FireDisable = false;
    FireDisable2 = false;
    end = false;
    start = true;
  }
  UI();
  Music();
  resetDir();
  //cur_dir = 0;
}

//|| Draw Scripts above || Bullet scripts underneath || || Draw Scripts above || Bullet scripts underneath || || Draw Scripts above || Bullet scripts underneath ||

void bullet(){for (int i=0, array = 0; i<Bullets+1; ++i,++array) {
    if (BulletsHP[array] > 0) {
      BulletsX[array]+= BulletsXV[array];
      BulletsY[array]+= BulletsYV[array];
      goTo(BulletsX[array],BulletsY[array]);
      turn(atan2(BulletsXV[array],BulletsYV[array])*-1+(PI/2));
      image(Bullet,-12.5,-7.5);
      resetDir();
      goTo(0,0);
        for (int i2=0, array2 = 0; i2<15; ++i2,++array2){
          if (astHP[array2] > 0) {
            if (abs(astX[array2]-BulletsX[array])<45){
            if (abs(astY[array2]-BulletsY[array])<45 && end == false){
              tempCount = astHP[array2];
              astHP[array2]-=BulletsHP[array];
              BulletsHP[array]-=tempCount;
          }}}}
      if (BulletsX[array]>width+50){BulletsHP[array]=-1;}
    }
  }
}

void bomb(){
  fireDelay2 -= 0.05;
  fireDelay3 -= 0.05;
  fireDelay5 -= 0.05;
  SSEC -= 0.05;
  TP -= 0.05;
  if (SSEC < 0){SSEC=0;}
  if (ShipMode == 1 || ShipMode == 1.2){
  if (ControlKey("B") && fireDelay2<0 && FireDisable == false && FireDisable2 == false){
    Bombs++;
    BombsX[Bombs] = x1;
    BombsY[Bombs] = y1;
    fireDelay2 = 3;
    BombsHP[Bombs] = 1;
  }}else if(ShipMode == 2 || ShipMode == 2.2){
  if (ControlKey("B") && fireDelay3<0 && FireDisable == false && FireDisable2 == false){
    SSEC = 9;
    fireDelay3 = 30;
  }}else if(ShipMode == 3 || ShipMode == 3.2){
  if (ControlKey("B") && fireDelay5<0 && FireDisable == false && FireDisable2 == false){
    TP = 30;
    fireDelay5 = 50;
  }}
  if (SSEC > 0){
    goTo(x1,y1);
    turn(random(-180,180));
    if (round(random(0,3))==1){
      image(SSEC0,-75,-75);
    }else if (round(random(0,2))==1){
      image(SSEC1,-75,-75);
    }else{
      image(SSEC2,-75,-75);
    }
    resetDir();
    goTo(0,0);
        for (int i2=0, array2 = 0; i2<15; ++i2,++array2){
              if (astHP[array2] > 0) {
            if (dist(astX[array2],astY[array2],x1,y1) < 125 && end == false){
              astHP[array2]-=45;
            }
              }
        }
  }
    for (int i=0, array = 0; i<Bombs+1; ++i,++array) {
    if (BombsHP[array] > 0) {
      BombsX[array]+= 2;
      makeImage(Bomb,BombsX[array]-12.5,BombsY[array]-12.5);
        for (int i2=0, array2 = 0; i2<15; ++i2,++array2){
          if (astHP[array2] > 0) {
            if (abs(astX[array2]-BombsX[array])<45){
            if (abs(astY[array2]-BombsY[array])<45 && end == false){
              ExpX[array] = BombsX[array];
              ExpY[array] = BombsY[array];
              ExpHP[array] = 10;
              BombsHP[array]=-1;
          }}}}
      if (BombsX[array]>width+50){BombsHP[array]=-1;}
    }
        for (int i2=0, array2 = 0; i2<15; ++i2,++array2){
            if (ExpHP[array] > 0){
              ExpHP[array] -= 0.01;
              image(Explosion,ExpX[array]-(150/2),ExpY[array]-(150/2));
              if (astHP[array2] > 0) {
            if (dist(astX[array2],astY[array2],ExpX[array],ExpY[array]) < 100 && end == false){
              astHP[array2]-=1;
            }
              }
             if (dist(x1+50,y1+50,ExpX[array],ExpY[array]) < 100 && end == false){
             Damage();
             }
          }
        }
    }
  
}

// || Bullet Scripts above || UI scripts underneath || || Bullet Scripts above || UI scripts underneath || || Bullet Scripts above || UI scripts underneath ||

void UI(){
  textAlign(RIGHT);
  fill(255/2,126);
tint(255/2, 126); 
  makeText(points,width-25+3,50+3);
  //if (HighScore == null){makeText("Err",width-25+3,100+3);
  //}else{makeText(HighScore,width-25+3,100+3);}
  fill(255);
  noTint();
  makeText(points,width-25,50);
  textAlign(CENTER);
  if (ShipMode == 1 || ShipMode == 1.2){
  image(Laser,LaserX,y1-15+(sin(radians(millis()*2))*-2),3000,10+(sin(radians(millis()*2))*4));
  image(Laser,LaserX,y1+5+(sin(radians(millis()*2))*-2),3000,10+(sin(radians(millis()*2))*4));
  }else if (ShipMode == 2 || ShipMode == 2.2){
  for (float i=1, array = 0; array<15; ++array){
    if (astX[int(array)]<astX[int(i)]){
    i = array;}
    if (array==14){tempCount = i;}}
  goTo(LaserX,y1-5);
  turn(atan2(astX[int(tempCount)]-LaserX,astY[int(tempCount)]-y1)*-1+(PI/2));
  image(Laser,0,-5+(sin(radians(millis()*2))*-2),3000,10+(sin(radians(millis()*2))*4));
  if (LaserX>0){astHP[int(tempCount)]-=0.05;}
  resetDir();
  turn(-cur_dir);
  goTo(0,0);
  }
  resetDir();
  goTo(x1,y1);
  //turn(radians(moveDir*-4));
  makeImage(/*loadImage("Power3.png")*/UFO,-50,-50);
  resetDir();
  goTo(0,0);
  //text(moveDir*2,width/2,height-20);
  makeText(ShipMode,x1,y1);
  Inv -= 0.05;
  
    if (h1 >= 0){
      makeImage(Hearts,25,25);
    }else{
      makeImage(Lost,25,25);
    }
    if (h2 >= 25){
      makeImage(Hearts,100,25);
    }else{
      makeImage(Lost,100,25);
    }
    if (h3 >= 100){
      makeImage(Hearts,175,25);
    }else{
      makeImage(Lost,175,25);
    }
  textFont(Futuristic);
  //text(type,10,200);
    if (shield >= 1){
      makeImage(Shield,250,25);
    }else{
      makeImage(ShieldInactive,250,25);
    }
    if (shield >= 2){
      makeImage(Shield,325,25);
    }else{
      makeImage(ShieldInactive,325,25);
    }
    if (shield >= 3){
      makeImage(Shield,400,25);
    }else{
      makeImage(ShieldInactive,400,25);
    }
    if (shield > 3){
      fill(200,50,50);
      textSize(20);
      textAlign(CENTER);
      //text("MAX FIVE SHIELDS.", width/2,30);
      shield = 3;
    }
  if (true){
    fill(125);
  strokeWeight(5);
    stroke(5);
    rect(500,25,200,25);
    rect(500,50,200,25);
    fill(0, 251, 255);
      if (ShipMode == 1 || ShipMode == 1.2){
    fill(0, 251, 255);
      }else{
    fill(25,225,25);
      }
  strokeWeight(0);
    if (stun > 0){
      rect(503,28,stun/10*195,20);
    }else{
      rect(503,28,10/10*195,20);
    }
      if (ShipMode == 2 || ShipMode == 2.2){
    fill(0, 251, 255);
      }else{
    fill(25,225,25);
      }
    OSS-=0.01;
    if (OSS < 0){OSS=0;}
    if (OSS > 0){
      if (OSS > 10){
        rect(503,53,130,20);
      fill(225,25,25);
        rect(130+503,53,(OSS-10)/15*195,20);
      }else{
        rect(503,53,OSS/15*195,20);
    } }else{
        rect(503,53,130,20);
      fill(225,25,25);
        rect(130+503,53,195/3,20);
    }
    stroke(0);
  strokeWeight(1);
  
    fill(125);
  strokeWeight(5);
    stroke(5);
    //rect(700,100,100,50);
    //rect(800,100,100,50);
    if (fireDelay2 > 0){
    ellipse(480,35,20,20);
    }else{
      if (ShipMode == 1 || ShipMode == 1.2){
    fill(225,25,25);
      }else{
    fill(25,225,25);
      }
    ellipse(480,35,20,20);
    }
    fill(125);
    if (fireDelay3 > 0){
    ellipse(480,65,20,20);
    }else{
      if (ShipMode == 2 || ShipMode == 2.2){
    fill(225,25,25);
      }else{
    fill(25,225,25);
      }
    ellipse(480,65,20,20);
    }
    fill(225,25,25);
  strokeWeight(0);
    /*if (fireDelay > 0){
      rect(703,103,fireDelay/0.3*95,45);
    }else{
      rect(703,103,0.3/0.3*95,45);
    }
    if (fireDelay2 > 0){
      rect(803,103,fireDelay2/3*95,45);
    }else{
      rect(803,103,3/3*95,45);
    }*/
    stroke(0);
  strokeWeight(1);
  }
  fill(256,256,256);
  rect(0,startY,width,height);
  image(Makerspace,width/2-134,startY+100);
  textSize(50);
  fill(0);
  if(startY==0){x1=width/4;y1=height/2;}
  //text("Made by Gunnar, Leonid and Herman",width/2-400,startY+500);
  //text("K to change between SNES and XBox controls XBox is default",width/2-600,startY+1000);
  textAlign(CENTER);
  text("Made by Gunnar, Leonid and Herman",width/2,startY+500);
  textSize(25);
  text("K to change between SNES and XBox controls XBox is default",width/2,startY+1000);
  }
    
// || UI Scripts above || Damaged scripts underneath || || UI Scripts above || Damaged scripts underneath || || UI Scripts above || Damaged scripts underneath ||

void Damage()
{
  if (Inv < 0){
  if (shield > 0 && !(OSS > 10)){
    shield -= 1;
    Inv = 3;
  } else if (hp == 3){
    hp -= 1;
    h3 = -100;
  } else if (hp == 2){
    hp -= 1;
    h2 = -100;
  } else if (hp == 1){
    hp -= 1;
    h1 = -100;
    gravity = 0.1;
    ms = 0;
    y1 = -100000;
    x1 = -100000;
    SSY = -2000;
    BSSY = 40;
    end = true;
  }
  else{
    h1 = -100;
    h2 = -100;
    h3 = -100;
    gravity = 0.2;
    ms = 0;
    y1 = -100000;
    x1 = -100000;
    SSY = -2000;
    BSSY = 40;
    end = true;
  }
  }
}

// || Damaged scripts above || Shooting scripts underneath || || Damaged scripts above || Shooting scripts underneath || || Damaged scripts above || Shooting scripts underneath ||

void shootScripts(){
fireDelay -= 0.05;
  if (ShipMode == 1 || ShipMode == 1.2){
  if (ControlKey("A") && fireDelay<0 && FireDisable == false && FireDisable2 == false){
    Bullets++;
    BulletsX[Bullets] = x1;
    BulletsY[Bullets] = y1;
    BulletsXV[Bullets] = 5;
    BulletsYV[Bullets] = 0;
    fireDelay = 0.3;
    BulletsHP[Bullets] = 1;
  }
  }else if (ShipMode == 2 || ShipMode == 2.2){
  if (ControlKey("A") && fireDelay<0 && FireDisable == false && FireDisable2 == false){
  for (float i=-10; i!=11; ++i){
    tempCount = (atan2(sin(radians((moveDir*-4)*-0.5+0)),cos(radians((moveDir*-4)*0.5+0)))*-1+(PI/2*2))+radians(i-0*2);
    Bullets++;
    BulletsX[Bullets] = x1;
    BulletsY[Bullets] = y1;
    //BulletsXV[Bullets] = (radians(sin(tempCount))*-1+(PI/2))*5;
    //BulletsYV[Bullets] = (radians(cos(tempCount))*-1+(PI/2))*5;
    BulletsXV[Bullets] = sin(tempCount)*15;
    BulletsYV[Bullets] = cos(tempCount)*15*-1;
    fireDelay = 0.3;
    BulletsHP[Bullets] = 0.05;
    }
  }
  }else if (ShipMode == 3 || ShipMode == 3.2){
  if (ControlKey("Y") && fireDelay4<0 && FireDisable == false && FireDisable2 == false){
    fireDelay = 5;
    Drone++;
  }
  }
  
  
  
    if (ControlKey("Y") && FireDisable2 == false){
      LaserX = x1;
      FireDisable = true;
  }
  else
  {
    FireDisable = false;
    LaserX = -width*99;
  }
  
  stun -= stuntime;
  if (ShipMode == 1 || ShipMode == 1.2){
    if (ControlKey("X") && end == false && FireDisable == false && stuntime == 0 && !(OSS > 10)){
        stun = 10;
        stuntime = 0.2;
  }
  }else if (ShipMode == 2 || ShipMode == 2.2){
    if (ControlKey("X") && end == false && FireDisable == false && stuntime == 0 && !(OSS > 0)){
      OSS=15;
    }
  }
  if (stun > 0){
    textSize(10);
    ms = 0;
    //text("Charging",x1-10,y1+75);
    FireDisable2 = true;
  }
  if (stun < 0 && end == false){
    ms = 5;
    shield += 1;
    stuntime = 0;
    stun = 0;
    FireDisable2 = false;
  }
}
void Music()
{
   if ( player.position() == player.length() )
  {
    player.rewind();
    player.play();
  }
  else
  {
    player.play();
  }
}
