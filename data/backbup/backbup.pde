float x = 0;
float y = 0;
int sc = 69;

void setup() {
  size(600, 400);
  strokeWeight(5);
}

void draw() {
  line(x, y, x, y + 50);
  line(x, y, x + 50, y);
  for(int i=0; i<sc; i++) {
    point(x, y);
    point(x - 50, y);
    point(x, y - 50);
    x += 1;
    y += 1;
  }}

void keyPressed() {
  background(200);
  if(keyCode == UP) sc = sc + 1;
  if(keyCode == DOWN) sc = sc - 1;
  x = 0;
  y = 0;
}
